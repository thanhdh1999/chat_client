import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Phonebooks from '../views/Phonebooks.vue'
import Userhistory from '../views/Userhistory.vue'
import DetailUser from '../views/DetailUser.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/auth/Login',
      name: 'auth.Login',
      component: () => import('../views/auth/Login.vue')
    },
    {
      path: '/auth/register',
      name: 'auth.register',
      component: () => import('../views/auth/Register.vue')
    },
    {
      path: '/phonebooks',
      name: 'phonebooks',
      component: Phonebooks
    },
    {
      path: '/details',
      name: 'details',
      component: Userhistory
    },
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/detailsUser',
      name: 'detailsUser',
      component: DetailUser
    }
  ]
})

export default router
