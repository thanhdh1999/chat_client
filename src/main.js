import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import axios from "axios";
import Swal from "sweetalert2";
window.axios = axios;
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
