import axios from "axios";

const appService = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
});

// Thêm interceptor để xác thực yêu cầu
appService.interceptors.request.use((config) => {
    const token = localStorage.getItem("authToken");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
});

class AppService {
    // login user
    login(data) {
        return appService.post("api/auth/login", data)
            .then((response) => {
                const token = response.data.token;
                // Lưu trữ token vào local storage
                localStorage.setItem("authToken", token);
                return token;
            });
    }
    // danh sách user
    Users() {
        return appService.get("api/user/");
    }
    // chi tiết user đang đăng nhập
    DetailUser(){
        return appService.get("api/user/detail");
    }
    // edit user đang login
    EditUser(details) {
        return appService.post("api/user/edit", details);
    }
    // Lấy ra tin nhắn của user đang đăng nhập và user được nhắn tin (id được nhắn tin là id truyền vào)
    GetMess(id) {
        return appService.get("api/message/"+id);
    }
    //  xóa tin nhắn được chọn(của user đang đăng nhập0
    deleteMess(id){
        return appService.delete("api/message/delete/" +id);
    }
    // user đang login sửa tin nhắn của mình
    updateMess(id,data){
        return appService.post("api/message/update/" +id,data);
    }
    //  lấy ra danh sách danh bạ
    Phonebooks() {
        return appService.get("api/phonebooks");
    }
    // lấy ra chi tiết của user trong danh bạ theo id truyền vào
    detailphonebooks(id) {
        return appService.get("api/phonebooks/details/"+id);
    }
    //  thêm tin nhắn mới giữa user đang đăng nhập gửi cho user được chọn (rep_id)
    CreateMess(repId, data) {
        return appService.post("api/message/create/" + repId,data);
    }
    // tìm user bất kỳ theo danh sách user để nahwns tin
    searchUsers(search_query){
        return appService.get("api/user/search?keyword="+search_query );
    }
}

export const httpService = new AppService();
